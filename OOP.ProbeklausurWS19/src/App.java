public class App {

    public static void main(String[] args) throws Exception {

        interface Boolable{
            MyBool not();
            MyBool and(MyBool b);
            default MyBool nand(MyBool b){
                return and(b).not();
            }
        }
    
        enum MyBool implements Boolable{
            TRUE, FALSE;
    
            public MyBool not(){
                return this == TRUE ? FALSE : TRUE;
            }
            public MyBool and(MyBool b){
                if(this == b) return b;
                return FALSE;
            }
            
        }
    
        class Claim{
            MyBool[] bools;
            Claim(MyBool... bools){
                this.bools = bools;
            }
            public MyBool andAll(){
                assert bools.length <= 1;
                MyBool res = bools[0].and(bools[1]); 
                for(int i = 1; i < bools.length; i++){
                    res.and(bools[i]);
                }
                return res;
            }
            @Override
            public String toString() {
                String a = "";
                for(int i = 0; i < bools.length; i++){
                    a += bools[i].name();
                }
                return a;
            }
        }
        
        MyBool a = MyBool.TRUE;
        MyBool b = MyBool.TRUE;
        MyBool c = MyBool.FALSE;
        MyBool d = MyBool.TRUE;
        


        Claim e = new Claim(a, b, c, d);
    }
}
