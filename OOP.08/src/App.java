class Node{
    String item; // "+", "<<"
    Node[] children;

    public String toString(){
        String s = "(" + item;
        for(Node child : children)
            s += " " + child;
        return s + ")";
    }
}

class UnaryNode extends Node {
    UnaryNode(String item, Node onlyOne){
        this.item = item;
        children = new Node[]{onlyOne};
    }
}

class BinaryNode extends Node {
    BinaryNode (String item, Node left, Node right) {
        this.item = item;
        children = new Node[]{left, right};
    }
}

class TernaryNode extends Node{
    TernaryNode(String item, Node first, Node second, Node third) {
        this.item = item;
        children = new Node[]{first, second, third};
    }
}

class TerminalNode extends Node{
    TerminalNode(String item) {
        this.item = item;
    }
    public String toString(){
        return item;
    }
}

