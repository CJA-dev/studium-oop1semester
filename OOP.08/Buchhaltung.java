/*
Lernziele:
* Arrays gehen nicht nur mit primitiven Typen
* Man kann ein Objekt nicht löschen: hier Lösung durch Flag
* Unterscheidung von statischen und nicht-statischen Methoden
*/

class Account {
    static Account[] accounts = new Account[3];
    static int fillLevel = 0;

    static boolean isNumberAvailable(int number) {
        assert number >= 0 : "Negative account numbers aren't allowed";
        for (int i = 0; i < fillLevel; i++) {
            if (accounts[i].number == number)
                return false;
        }
        return true;
    }
    static Account getAccount(int number) {
        for (Account account : accounts) {
            if (account.number == number)
                return account;
        }
        return null;
    }
    static int[] getNumbers() {
        int counter = 0;
        for (Account account : accounts)
            if (account != null) counter++;
        int[] numbers = new int[counter];
        counter = 0;
        for (Account account : accounts)
            if (account != null) numbers[counter++] = account.number;
        return numbers;
    }

    int balance = 0;
    int number = 0;
    int credit = 0;
    boolean isClosed = false;

    Account(int accountNumber) {
        assert fillLevel < accounts.length : "No space for further accounts";
        assert isNumberAvailable(accountNumber) : "Account number in use";
        number = accountNumber;
        accounts[fillLevel++] = this;
        if (fillLevel > accounts.length)
            fillLevel = accounts.length;
    }

    void delete() {
        assert !isClosed : "Account is closed";
        assert balance == 0 : "Balance is not zero";
        for (int i = 0; i < fillLevel; i++) {
            if (accounts[i] == this) {
                accounts[i] = accounts[--fillLevel];
                accounts[fillLevel] = null;
                isClosed = true;
                return;
            }
        }
        assert false : "Fatal error: Tried to delete unlisted account";
    }
    void setCredit(int amount) {
        assert !isClosed : "Account is closed";
        assert amount > 0 : "Credit is zero or negative";
        credit = amount;
    }
    int getCredit() {
        assert !isClosed : "Account is closed";
        return credit;
    }
    int currentBalance() {
        assert !isClosed : "Account is closed";
        return balance;
    }
    void deposit(double amount) { // Einzahlung
        assert !isClosed : "Account is closed";
        assert amount > 0 : "Amount is not positive";
        balance += amount;
    }
    boolean withdraw(int amount) { // Auszahlung
        assert !isClosed : "Account is closed";
        assert amount > 0 : "Amount is not positive";
        assert balance - amount >= -credit : "Credit overdraft";
        if (balance >= amount) {
            balance -= amount;
            return true;
        }
        return false;
    }
    public String toString() {
        return isClosed ? "closed" : String.format("(%s: %d.%02d€)", number, balance / 100, balance % 100);
    }
}
