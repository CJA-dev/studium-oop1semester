public class App{
    
    static class Bar{                                              //* class to represent one Bar
        int size = 0;                       // size of Bar                
        static final int MAX_SIZE = 40;     // const: max length of one bar
        Bar(int size){                      // Constructor: with given size
            assert size < MAX_SIZE == true : "Der übergebene Wert ist zu groß.";
            assert size > 0 == true : "Der übergebene Wert muss größer als Null sein.";              
    
            this.size = size;
        }

        void print(){
            for(int i = 0; i < this.size; i++) System.out.println("#");;
        }
    
        @Override
        public String toString(){           // return Bar with size of #
            String res = "";
            for(int i = 0; i < this.size; i++) res += "#";
            res += " " + this.size;
            return res;
        }
    }    
    
    static class Chart{                                     //* class to represet Char with bars
    
        Bar bars[] = new Bar[0];        // array: store generated bars
        int cursor = 0;
    
        Chart(int sizeBars){                                //* create empty array for Bars
            this.bars = new Bar[sizeBars];
        }
    
        Chart(Bar... args){                                 //* Contructor: variable count of Bar elements
            for(Bar arg : args){        // loop through given Bar parameters
                add(bars, arg);         // add new Bar for every Bar parameter
            }
            System.out.println(bars);
        }
    
        void up(){                                          //* move courser up
            try{
                if(this.cursor - 1 >= 0) this.cursor--;
            }catch(java.lang.ArrayIndexOutOfBoundsException e){
                this.cursor = 0;
                System.out.println("Hihihi");
            }                               
            this.toString();
        }
        void down(){                                        //* move cursor down
            try {
                if(this.cursor + 1 <= this.bars.length-1) this.cursor++;
            } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                this.cursor = this.bars.length-1;
                System.out.println("Hohoho");
            }
            this.toString();
        }
        void set(Bar bar){  
            this.bars[this.cursor] = bar; 
            this.toString();
        }
        // add Bar element to Bar Array
        private void add(Bar[] bars, Bar element){          //* add Bar element to Bar array
            System.out.println(this.bars.length);
            int newLength = bars.length + 1;        // increase Bar array about one
            Bar[] newBars = new Bar[newLength];     // create new Bar array
            newBars = bars;                         // set old bar array on new 
            newBars[newLength] = element;           // fill last array pos with element
            this.bars = newBars;                    // replace old array with new one
        }
        @Override   
        public String toString(){                           //* metho to print string
            String res = "\n";
            for(int i = 0; i < this.bars.length; i++){  // loop through 
                if(this.cursor == i) res += "* ";       // print cursor on current pos
                else res += "  ";                       // if no cursor -> space
                if(this.bars[i] == null) res += "-";    // if no bar on pos -> -
                else res += this.bars[i].toString();    // use .toSTring() to print bar on pos
                res += "\n";                            // line break after every bar
            }
            System.out.println(res);
            return res;
        } 
    
    }
    public static void main(String... args){
    
        Chart a = new Chart(3);
        // Chart b = new Chart(new Bar(12), new Bar(3));
        a.down();
        a.down();
        a.down();
        a.set(new Bar(5));
        a.toString();

        System.out.println(a.bars);
    }
}