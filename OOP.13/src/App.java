import java.net.http.HttpRequest;

/*
Erstellen Sie ein Interface namens `Positionable`, das positionierbare Dinge
auf einer Fläche (kartesischen Koordinatensystem) gedacht ist. Es gibt folgende Methoden:

* `getX` gibt den Positionswert als `double` für die x-Koordinate zurück
* `getY` gibt entsprechend den Positionswert für die y-Koordinate zurück
* `getDistance` liefert die Entfernung zwischen dem aktuellen und einen anderen `Positionable`

Erstellen Sie das Interface und geben Sie eine `default`-Implementierung für `getDistance` an.
*/

interface Positionable{
    double getX();
    double getY();
    default double getDistance( Positionable other){
        return Math.sqrt(
            Math.pow((other.getX() - this.getX()), 2) + 
            Math.pow((other.getY() - this.getY()), 2)
            );
    }
}


/*
Implementieren Sie die Klasse `Pos` als `Positionable`. Die folgende Beispielausgabe ist dabei zu berücksichtigen:

```
jshell> new Pos(2,3)
$152 ==> Pos(2.0, 3.0)
```

*/

class Pos implements Positionable{
    final private double x;
    final private double y;
    Pos(double x, double y){
        this.x = x;
        this.y = y;
    }
    public double getX(){ return x; }
    public double getY(){ return y; }
    
    @Override
    public String toString(){
        return "Pos(" + getX() + ", " + getY() + ")";
    }
}

/*
Das Interface `GraphicalOperations` ist für graphische Flächenelemente gedacht.
Beispiele sind ein Rechteck oder ein Kreis. Für solche Elemente kann die
Koordinate des Mittelpunkts und der Radius des Kreises angegeben werden, der
vom Mittelpunkt ausgehend die Aussenhülle des Elements beschreibt. Zwischen
zwei graphischen Elementen kann ihre Distanz ermittelt und festgestellt werden,
ob sie -- bezogen auf die Außenhülle -- sich überlappen, sprich "kollidieren".

*/
interface GraphicalOperations {
    Positionable getCenter();
    double getBorderRadius();
    default double getDistance(GraphicalOperations other){
        return getCenter().getDistance(other.getCenter());
    };
    default boolean isColliding(GraphicalOperations other){
        double restDistance = getDistance(other) - getBorderRadius() - other.getBorderRadius();
        return restDistance <= 0 ? true : false;
    };
}
/* 

Setzen Sie `getDistance` und `isColliding` als `default`-Methoden um.

*/



/*
Implementieren Sie das Interface `GraphicalOperations` für die Klasse `Square`
und `Circle`. In einem zweiten Schritt lagern Sie Gemeinsamkeiten in einer
abstrakten Oberklasse namens `PositionableObject` aus.
*/

class Square implements GraphicalOperations{
    Pos pos;
    double length;

    Square(double x, double y, double length){ // x and y is bottom left of object
        pos = new Pos(x, x);
        this.length = length;
    }

    public Positionable getCenter(){ return new Pos(pos.getX() + length/2, pos.getY() + length/2 ); } //! only for object in first quarter
    public double getBorderRadius(){ return length  }
}

class Circle implements GraphicalOperations{
    Pos pos;
    double radius;

    Circle(double x, double y, double r){
        pos = new Pos(x, y);
        this.radius = r;
    }

    public Positionable getCenter(){ return pos; }
    public double getBorderRadius(){ return radius;}

}