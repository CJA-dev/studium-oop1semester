class Canvas {
    final char[][] pane;
    
    float xmin, xmax; //* benötigte X, Y Variablen
    float ymin, ymax;
    float deltaX, deltaY;

    //* Constructor: create Canvas with give values
    Canvas(int width, int height) {
        assert width > 0 && height > 0; // check gives val's
        pane = new char[width][height]; // create 2d-array with given val's
        clear();                        // clear field
        setRange(-1,+1,-1,+1);          // definiere Größe des Feldes. Setze minimal und maximal Werte.
    }

    //* Method: clear field
    void clear() {
        // alle Werte der `pane` sollen auf '.' gesetzt werden.
        for(int i = 0; i < pane.length; i++){
            for(int j = 0; j < pane[i].length; j++){
                pane[i][j] = '.';
            }
        }
    }
    
    int getWidth() { return pane.length;}       // get widht of field
    int getHeight() { return pane[0].length;}   // get height of field

    void setRange(float xmin, float xmax, float ymin, float ymax) {
        assert xmin < xmax && ymin < ymax;
        this.xmin = xmin;
        this.xmax = xmax;  
        this.ymin = ymin;
        this.ymax = ymax;
        deltaX = Math.abs(xmax - xmin) / (getWidth()  - 1);
        deltaY = Math.abs(ymax - ymin) / (getHeight() - 1);
    }
    
    int getX(float fx) {
        return Math.round((fx - xmin) / deltaX);
    }
    int getY(float fy) {
        return Math.round((ymax - fy) / deltaY);
    }
    
    char getPoint(float fx, float fy) {
        if(fx >= xmin && fx <= xmax && fy >= ymin && fy <= ymax){
            return pane[getX(fx)][getY(fy)];
        }
        return ;
    }
    Canvas setPoint(float fx, float fy, char c) {
        // Punkte außerhalb der Zeichenfläche werden ignoriert.
        if(fx >= xmin && fx <= xmax && fy >= ymin && fy <= ymax){
            pane [getX(fx)][getY(fy)] = c;
        }
        return this;
    }
    public String toString() {
        // Schauen Sie sich den Testfall an.
        String res = "";
        for(int i = 0; i < pane.length; i++){
            for(int j = 0; j < pane[i].length; j++){
                res += pane[i][j];
            }
            res += "\n";
        }
        return res;
    }
    void show() { System.out.print(this); }
}
