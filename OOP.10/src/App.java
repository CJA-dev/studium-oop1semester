import java.util.Objects;


interface Stackable{
    boolean isEmpty();
    default int top(){ throw new UnsupportedOperationException(); }
    default Stackable pop(){ throw new UnsupportedOperationException(); }
    Stackable push(int element);
}
class StackWithElements implements Stackable{
    
    private int element;
    private Stackable previous;

    static Stackable of(int... elements){
        Stackable s = new EmptyStack();
        for(int element : elements){
            s = s.push(element);
        }
        return s;
    }

    StackWithElements(Stackable previous, int element){
        assert Objects.nonNull(previous); //* setzt voraus, dass das übergebene Object nicht leer ist
        this.previous = previous;
        this.element = element;
    }
    public boolean isEmpty(){ return false; }
    public Stackable push(int element){
        return new StackWithElements(this, element);
    }
    public int top(){ return element;}
    public Stackable pop(){ return previous;}
    
    @Override
    public String toString(){
        return pop() + " <- " + top();
    }
}

class EmptyStack implements Stackable{
    public boolean isEmpty(){ return true; }
    public Stackable push(int element){
        return new StackWithElements(this, element);
    }
    
    @Override
    public String toString(){
        return "[ ]";
    }
}