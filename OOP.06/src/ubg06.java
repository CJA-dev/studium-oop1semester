class Account{
    // Ausgangsbetrag
    double balance = 0;

    // methode actualBalance -> aktueller Kontostand
    double actualBalance(){
        return this.balance;
    }
    // void deposit(betrag) -> erhöht Kontostand um betrag
    void deposit(double value){
        this.balance = this.balance + value;
    }
    // methode boolean disburse(betrag) -> reduziert konto um betrag
    // return true : betrag < balance : false
    boolean disburse(double value){
        if( (this.balance - value) >= 0.0 ){
            this.balance = this.balance - value;
            return true;
        }
        return false;
    }
    public String toString() {
        return String.format("%+.2f€", balance);
    }
}