String toStringWithDoots(int a){
    // split input in new char array(a)
    char[] aChar = Integer.toString(a).toCharArray();
    // a to String
    String res = "";

    for(int i = aChar.length; i > 0; i--){
        if(i % 3 == 0 && res != "") res += ".";
        res += aChar[aChar.length - i];
    }
    return res;
}

assert toStringWithDoots(123456).equals("123.456");
assert toStringWithDoots(23456).equals("23.456");