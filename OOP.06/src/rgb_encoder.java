class Coordinate {
    final int x;
    final int y;
    Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }
    float distance(Coordinate other) {
        return (float)Math.sqrt((other.x - x) * (other.x - x) + (other.y - y) * (other.y - y));
    }
    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

}

assert new Coordinate(2,3).toString().equals("(2, 3)") : "instantiation";
assert new Coordinate(1,0).distance(new Coordinate(3,0)) == 2.0f : "vertical";
assert new Coordinate(0,1).distance(new Coordinate(0,3)) == 2.0f : "horizontal";
assert new Coordinate(0,0).distance(new Coordinate(1,1)) == (float)(Math.sqrt(2)) : "Square";

class RGB {
    byte red, green, blue;

    static int encodeColor(int red, int green, int blue) {
        return ((red & 0xFF) << 16) | ((green & 0xFF) << 8) | (blue & 0xFF);
    }
    static int decodeRed(int colorCode) {
        return (colorCode & 0b11111111_00000000_00000000) >> 16;
    }
    static int decodeGreen(int colorCode) {
        return (colorCode & 0b11111111_00000000) >> 8;
    }
    static int decodeBlue(int colorCode) {
        return colorCode & 0b11111111;
    }

    RGB(int colorCode) {
        red = (byte)decodeRed(colorCode);
        green = (byte)decodeGreen(colorCode);
        blue = (byte)decodeBlue(colorCode);
    }
    void setColor(int colorCode) {
        red = (byte)decodeRed(colorCode);
        green = (byte)decodeGreen(colorCode);
        blue = (byte)decodeBlue(colorCode);
    }
    int getColor() {
        return encodeColor(red, green, blue);
    }
    @Override
    public String toString() {
        return String.format("0x%06X", encodeColor(red, green, blue));
    }
}

assert RGB.encodeColor(0xff, 0x23, 0x11) == 0xff2311 : "rgb encoding";
assert RGB.decodeRed(0xff2311) == 0xff : "decoding red";
assert RGB.decodeGreen(0xff2311) == 0x23 : "decoding green";
assert RGB.decodeBlue(0xff2311) == 0x11 : "decoding blue";
assert new RGB(0xff2311).toString().equals("0xFF2311");
assert new RGB(0x2311).toString().equals("0x002311");
assert new RGB(0).toString().equals("0x000000");

class Pixel {
    Coordinate position;
    RGB color;

    Pixel(Coordinate pos, RGB color) {
        position = pos;
        this.color = color;
    }
    Pixel(Coordinate pos) {
        this(pos, new RGB(0));
    }
    Pixel() {
        this(new Coordinate(0, 0));
    }

    float distance(Pixel other) {
        return position.distance(other.position);
    }

    @Override
    public String toString() {
        return position + "[" + color + "]";
    }
}

assert new Pixel(new Coordinate(2, 3), new RGB(0xFF2211)).toString().equals("(2, 3)[0xFF2211]");
assert new Pixel(new Coordinate(2, 3)).toString().equals("(2, 3)[0x000000]");
assert new Pixel().toString().equals("(0, 0)[0x000000]");
assert new Pixel(new Coordinate(2, 3)).distance(new Pixel(new Coordinate(2, 3))) == 0f;
